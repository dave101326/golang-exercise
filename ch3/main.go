package main

import (
	"fmt"
)

func main() {
	vals := []float64 { 98, 93, 77, 82, 83 }
	fmt.Println(avg(vals))

	fmt.Println(multi_return())

	fmt.Printf("Gcd of 10 and 4: %d\n", Gcd(10, 4))

	fmt.Println(Sum(1, 2))          // 3
    fmt.Println(Sum(1, 2, 3))       // 6
    fmt.Println(Sum(1, 2, 3, 4))    // 10
	fmt.Println(Sum(1, 2, 3, 4, 5)) // 15
	numbers := []int{1, 2, 3, 4, 5}
    fmt.Println(Sum(numbers...)) // 15

	fmt.Println(factorial(5))

	getX, setX := x_getter_setter(10)
    fmt.Println(getX()) // 10
    setX(20)
    fmt.Println(getX()) // 20
}

func avg(vals []float64) float64 {
	total := 0.0
	for _, val := range vals {
		total += val
	}
	return total/float64(len(vals))
}

func multi_return() (int, int) {
    return 3, 7
}

// return value with name
func Gcd(m, n int) (gcd int) {
    if n == 0 {
        gcd = m
    } else {
        gcd = Gcd(n, m%n)
    }
    return
}

// Variadic Functions
func Sum(numbers ...int) int {
	var sum int
	for _, number := range numbers {
		sum += number
	}
	return sum
}

// recursion
func factorial(x uint) uint {
	if x == 0 {
		return 1
	}
	return x * factorial(x-1)
}

// Closure
func x_getter_setter(x int) (func() int, func(int)) {
    getter := func() int {
        return x
    }
    setter := func(n int) {
        x = n
    }
    return getter, setter
}