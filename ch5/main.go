package ch5

import (
	"fmt"
)

type Point struct {
	x, y int
}

type Node struct {
    point *Point
    next  *Node
}

func test() {
	point := struct{ x, y int} {10, 20}
	fmt.Println(point)
	point.x = 30
	point.y = 40
	fmt.Println(point)

	point2 := Point{ 10, 10}
	fmt.Println(point2)
	point3 := &point2
	fmt.Println(point3)

	node := new(Node)
	node.point = &Point{10, 20}
	node.next = new(Node)
	node.next.point = &Point{10, 30}
    fmt.Println(node.point)      // &{10 20}
    fmt.Println(node.next.point) // &{10 30}
}