package main

import (
	"fmt"
	"reflect"
	"strconv"
)

func main() {

	// for loop
	var forLoopX [5]int
	for _, value := range forLoopX {
		fmt.Println(value)
	}

	forLoopX[4] = 10
	fmt.Println("array values are: ", forLoopX)

	forLoopY := [5]float64 { 98, 93, 77, 82, 83 }
	for _, value := range forLoopY {
		fmt.Print(value, " ")
	}
	fmt.Println()

	// make(map[key-type]val-type).
	mapX := make(map[string]int)
	mapX["key"] = 10
	mapX["key2"] = 20
	fmt.Println(mapX)

	// The optional second return value when getting a value from a map indicates if the key was present in the map.
	mapValue, mapBool := mapX["Be"]
	fmt.Println(mapValue, mapBool)

	if mapValue, mapBool := mapX["key2"]; mapBool {
		fmt.Println(mapValue, mapBool)
	}

	// array  [n]type
	arr := [...]int{1, 2, 3, 4, 5} // array type: [5]int
	
	//slice []type
	var _ = []int{1, 2, 3, 4, 5} // slice type: []int

    s := arr[1:3]
    fmt.Println(reflect.TypeOf(arr))   // [10]int
	fmt.Println(reflect.TypeOf(s)) // []int
	s[0] = 20
    fmt.Println(s)
	fmt.Println(arr)
	
	slice := make([]int, 5, 10)
    fmt.Println(slice)       // [0 0 0 0 0]
    fmt.Println(len(slice))  // 5
	fmt.Println(cap(slice))  // 10

	slice2 := append(slice, 4, 5)
	fmt.Println(slice2)

	slice3 := make([]int, 12)
	copy(slice3, slice2)
	fmt.Println(slice3)
	fmt.Println(slice3[0:2])

	// range
	for i, num := range slice3 {
		if i == 5 {
			fmt.Println("index 5: " + strconv.Itoa(num))
		}
	}
}