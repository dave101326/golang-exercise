package main

// Import the input/output library
import (
	"fmt"
)
func main() {
	fmt.Println("Enter a word: ")
	var input string
	fmt.Scanf("%s", &input)
	output := input
	fmt.Println("Your answer: " + output)

	for i := 1; i <= 10; i++ {
		if i%2 == 0 {
			fmt.Println(i, " even")
		} else {
			fmt.Println(i, " odd")
		}
	}
	fmt.Println("我是分隔線")
	for i := 1; i <= 10; i++ {
		switch i%2 {
		case 0:
			fmt.Println(i, " even")
		case 1:
			fmt.Println(i, " odd")
		}
	}
}
