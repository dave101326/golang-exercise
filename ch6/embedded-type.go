  package ch6

  import (
	  "fmt"
  )
  
  type Person struct {
	  Name string
  }
  
  func (p *Person) Talk() {
	  p.Name = "Derick"
	  fmt.Println("Hi, my name is: ", p.Name)
  }
  
  type Android struct {
	  // normally we do like this
	  // Person person
	  // however we have Embedded types
	  Person
	  Model string
  }
  
  func emm() {
	  a := new(Android)
	  a.Person.Talk() //calling the talk function
  
	  // Since Person is a member of Android, that is Android is a person
	  a.Talk() //this is the is-a relationship through the transitive property
  
	  // If a person and can and andriod is-a person, then android can talk :)
  }
  
