package ch4

import (
	"fmt"
	"os"
)

func panic_check(err error) {
    if err != nil {
        panic(err)
    }
}

func panic1() {
	f, err := os.Open("/tmp/dat")
	panic_check(err)

	defer func() {
		if f != nil {
			f.Close()
		}
	} ()

	b1 := make([]byte, 5)
    n1, err := f.Read(b1)
    panic_check(err)

    fmt.Printf("%d bytes: %s\n", n1, string(b1))
}