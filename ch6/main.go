package ch6

import (
	"fmt"
)

type Int int
type Funtcint2 func(Int)

func (n Int) Times(f Funtcint2) {
	if n < 0 {
		panic("must > 0")
	}

	var i Int
	for i = 0; i < n; i++ {
		f(i)
	}
}

type IntList []int
type Funcint func(int)

func (lt IntList) ForEach(f Funcint) {
	for _, element := range lt {
		f(element)
	}
}

func main() {
	var lt IntList = []int{10, 20, 30, 40, 50}
	ff := func(element int) {
		fmt.Println(element)
	}
	lt.ForEach(ff)

	//lt2 := []int {10, 20, 30, 40, 50}
	// lt2.ForEach undefined (type []int has no field or method ForEach)
	// 因為 ForEach 是針對 Funcint 定義，而不是針對 []int
	//lt2.ForEach(func(ele int) {
	//	fmt.Println(ele)
	//})

	var x Int = 10
    x.Times(func(n Int) {
        fmt.Println(n)
	})
	
	// nil sample
	fmt.Println(findById("123").String())
    fmt.Println(findById("789").String())
}

type Acc struct {
	id string
	name string
	balance float64
}

func (ac *Acc) String() string {
	if ac == nil {
		return "<nil>"
	}

	return fmt.Sprintf("Account{%s %s %.2f}",ac.id, ac.name, ac.balance)
}

func findById(id string) *Acc {
	accs := []*Acc{ &Acc{"123", "Justin Lin", 10000}, &Acc{"456", "Kevin Lin", 500} }
	for i := 0; i < len(accs); i++ {
		if accs[i].id == id {
			return accs[i]
		}
	}
	return nil
}