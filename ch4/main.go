package ch4

import (
    "fmt"
)

func main() {
	// LIFO
	defer fmt.Println("deffered 1")    
    defer fmt.Println("deffered 2")
    fmt.Println("Hello, 世界")   
}