package main

import "fmt"

func ping(pings chan<- string, msg string) {
	pings <- msg
}

func pong(pings <-chan string, pongs chan<- string) {
	value := <-pings
	pongs <- value
}
/* Channel Dictions */
func main() {
	pings := make(chan string, 1)
	pongs := make(chan string, 1)

	ping(pings, "aaa")
	pong(pings, pongs)
	fmt.Print("abc")
	fmt.Println(<-pongs)
}
