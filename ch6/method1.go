package ch6

import (
    "errors"
    "fmt"
)

type Point struct {
	x, y int
}

type Account struct {
    id      string
    name    string
    balance float64
}

// method
func (ac *Account) Deposit(amount float64) {
    if amount <= 0 {
        panic("必須存入正數")
    }
    ac.balance += amount
}

// function
//func Deposit(account *Account, amount float64) {
//    if amount <= 0 {
//        panic("必須存入正數")
//    }
//    account.balance += amount
//}

// method
func (ac *Account) Withdraw(amount float64) error {
    if amount > ac.balance {
        return errors.New("餘額不足")
    }
    ac.balance -= amount
    return nil
}

// method
func (ac *Account) String() string {
    return fmt.Sprintf("Account{%s %s %.2f}",
        ac.id, ac.name, ac.balance)
}

// overload method
func (p *Point) String() string {
	return fmt.Sprintf("Point{%d %d}", p.x, p.y)
}
func method1() {
    account := &Account{"1234-5678", "Justin Lin", 1000}
    account.Deposit(500)
    account.Withdraw(200)
	fmt.Println(account.String()) // Account{1234-5678 Justin Lin 1300.00}
	
	point := &Point{ 10, 10 }
	fmt.Println(point.String())   // Point{10 20}

	// method as value:------
	account1 := &Account{"1234-5678", "Justin Lin", 1000}
	// Method expression
	deposit := (*Account).Deposit
	deposit(account1, 500)
	// Method value
	acct1Withdraw := account1.Withdraw
	acct1Withdraw(200)
}