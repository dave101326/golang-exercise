package main

import (
	"fmt"
	"runtime"
	"sync"
	"time"
)

func f(from string, wg *sync.WaitGroup) {
	defer wg.Done()

	for i := 0; i < 8; i++ {
		fmt.Println(from, ":", i)
		time.Sleep(time.Second)
	}
}

/* goroutine with sync.WaitGroup */
func main() {
	// check CPU
	fmt.Println(runtime.GOMAXPROCS(0))

	wg := new(sync.WaitGroup)
	//wg.Add(2) /* fatal error: all goroutines are asleep - deadlock! */
	wg.Add(1)

	go f("goroutine", wg)

	// anonymous function
	go func(msg string) {
		fmt.Println("anonymous goroutine: " + msg)
	}("going")

	time.Sleep(4 * time.Second)
	fmt.Println("done")

	wg.Wait()
}
