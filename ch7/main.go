package ch7

import (
	"fmt"
	"math"
)

type geometry interface {
	area() float64
	perim() float64
}

type rect struct {
	w, h float64
}

type circle struct {
	r float64
}

func (val rect) area() float64 {
	return val.w * val.h
}

func (val rect) perim() float64 {
	return 2 * val.w + 2 * val.h
}

func (val circle) area() float64 {
	return math.Pi * val.r * val.r
}

func (val circle) perim() float64 {
	return 2 * math.Pi * val.r
}

func measure(g geometry) {
	fmt.Println(g)
	fmt.Println(g.area())
	fmt.Println(g.perim())
}

func main() {
	fmt.Println("test")
	r := rect{3, 4}
	c := circle{5}

	measure(r)
	measure(c)

	// Type assertions 
    values := [...](interface{}){
        Duck{"Justin"},
        Duck{"Monica"},
        [...]int{1, 2, 3, 4, 5},
		map[string]int{"caterpillar": 123456, "monica": 54321},
		10,
    }

    for _, value := range values {
		switch v := value.(type) {
		case Duck:
			fmt.Println(v.name)
		case [5]int:
			fmt.Println(v[0])
		case int:
            fmt.Println(v)
		default:
            fmt.Println("非預期之型態")
		}
    }
}

type Duck struct {
    name string
}