package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
)

func printFile(f *os.File) (err error) {
	var r = bufio.NewReader(f)
	var line string

	for err == nil {
		line, err = r.ReadString('\n')
		fmt.Println(line)
	}

	return
}

func readFileAndPrint() {
	var fileName string
	fmt.Println("input file name:")
	//fmt.Scanf("%s", &fileName)
	//fmt.Println("your input:" + fileName)
	fileName = "C:\\Data\\test_file.txt"

	f, err := os.Open(fileName)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	// sample 1
	//bufio.NewReader(f).WriteTo(os.Stdout)

	// sample 2
	//printFile(f)

	//sample 3
	//w := bufio.NewWriter(os.Stdout)
	//w.ReadFrom(f)
	//w.Flush()

	// sample 4
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		panic(err)
	}

}

func fileCreate() {
	file, err := os.Create("testing.txt")

	if err != err {
		panic(err)
	}
	defer file.Close()

	file.WriteString("test")
}

func fileRead() {
	file, err := os.Open("testing.txt")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	stat, err := file.Stat()
	if err != nil {
		panic(err)
	}

	bs := make([]byte, stat.Size())
	_, err = file.Read(bs)
	if err != nil {
		panic(err)
	}

	str := string(bs)
	fmt.Println("content is: " + str)
}

func fileRead2() {
	content, err := ioutil.ReadFile("testing.txt")
	if err != nil {
		panic(err)
	}

	fmt.Printf("File contents: %s", content)
}

func main() {
	fileCreate()
	fileRead()
	fileRead2()
}
