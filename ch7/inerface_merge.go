package ch7

import (
	"fmt"
)

type PaerentTester interface {
	ptest()
}

type ChildTester interface {
	PaerentTester
	ctest()
}

type Subject struct {
	name string
}

func (s *Subject) ptest() {
	fmt.Printf("ptest %s\n", s)
}

func (s *Subject) ctest() {
	fmt.Printf("ctest %s\n", s)
}

func interface_merge() {
	var tester ChildTester = &Subject{"Test"}
	tester.ptest()
	tester.ctest()
}