package main

import (
	"fmt"
	"gitlab.com/dave101326/ch0/stringutil"
)

func main() {
	fmt.Println("Hello, world.")
	fmt.Println(stringutil.Reverse("!oG ,olleH"))
}